const express = require("express");
const mongoose = require("mongoose");
const bodyParser = require("body-parser");
const cors = require("cors");
const passport = require("passport");

// initialize server
const app = express();
const port = process.env.PORT || 3001;

// mongoose.connect("mongodb://localhost/bookingsystem",() =>{
// 	console.log("connected to db");
// });
mongoose.connect("mongodb+srv://admin:12341234@capstone3-6ttmj.mongodb.net/test?retryWrites=true&w=majority",() =>{
	console.log("connected to db");
});


// middlewares
app.use(passport.initialize());
app.use(cors());
app.use("/public",express.static("public/products"));
app.use(bodyParser.json());

mongoose.set('useFindAndModify', false);


app.use("/users",require("./routes/users"));
app.use("/categories",require("./routes/categories"));
app.use("/products",require("./routes/products"));
app.use("/transactions",require("./routes/transactions"));
app.use("/rooms",require("./routes/rooms"));
app.use("/rentedrooms",require("./routes/rentedrooms"));
app.use("/dates",require("./routes/dates"));
app.use("/messages",require("./routes/messages"));

app.use(function (err,req,res,next){
	res.status(400).json({
		err : err.message
	});
});

app.listen( port , () =>{
	console.log(`You are listening in port ${port}`)
})