const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const DateSchema = new Schema({
	reservationCode : {
		type : String,
		required : true
	},
	roomNumber : {
		type : String,
		required : true
	},
	productId : {
		type : String,
		required : true
	},
	dates : [
		{
			date : {
				type : Date
			}	
		}
	]
	
})

module.exports = mongoose.model("Dates",DateSchema)