const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const ProductSchema = new Schema({
	name : {
		type : String,
		required : [true, "Product name field is require"]
	},
	categoryId : {
		type : String,
		required : [true, "categoryId field is require"]
	},
	price : {
		type : Number,
		required : [true, "Product price field is require"]
	},
	description : {
		type : String,
		required : [true, "Product description field is require"]
	},
	image : {
		type : String,
		required : [true, "Product image field is require"]
	},
	noOfBedRooms : {
		type : Number,
		required: true
	},
	maxPerson : {
		type : Number,
		required : true
	},
	units : {
		type : Number,
		default : 0
	}
});

module.exports = mongoose.model("Product",ProductSchema);
