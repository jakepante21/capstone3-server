const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const RentedRoomSchema = ({
	productId : {
		type : String,
		required : true
	},
	type : {
		type : String,
		required : true
	},
	roomNumber : {
		type : String,
		required : true
	},
	status : {
		type : String,
		required : true
	},
	startDate : {
		type : Date,
		required : true
	},
	endDate : {
		type : Date,
		required : true
	},
	reservationCode : {
		type : String,
		required : true
	},
	userId : {
		type : String,
		required : true
	}
}) 

module.exports = mongoose.model("RentedRooms",RentedRoomSchema)