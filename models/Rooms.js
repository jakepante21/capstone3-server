const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const RoomSchema = new Schema({
	productId : {
		type : String,
		required : true
	},
	type : {
		type : String,
		required : true
	},
	roomNumber : {
		type : String,
		required : true
	},
	status : {
		type : String,
		default : "Available"
	}
})

module.exports = mongoose.model("Rooms",RoomSchema);