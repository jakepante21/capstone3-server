const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const TransactionSchema = new Schema({
	userId : {
		type : String,
		required : true
	},
	transactionCode : {
		type : String,
		required : true,
		unique : true
	},
	status : {
		type : String,
		default : "Pending"
	},
	paymentMode : {
		type : String,
		default : "Stripe"
	},
	products : [
		{
			productId : {
				type : String,
				required : true
			},
			roomNumber : {
				type : String,
				required : true
			},
			type : {
				type : String,
				required : true
			},
			name : {
				type : String,
				required : true
			},
			price : {
				type : Number,
				required : true
			},
			days : {
				type : Number,
				required : true
			},
			subtotal : {
				type : Number,
				required : true
			}
		}
	],
	startDate : {
		type : Date,
		required : true
	},
	endDate : {
		type : Date,
		required : true
	},
	createdAt : {
		type : Date,
		default : Date.now
	},
	total : {
		type : Number,
		required : true
	}
})

module.exports = mongoose.model("Transaction",TransactionSchema);
