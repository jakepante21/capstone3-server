const express = require("express");
const router = express.Router();
const Category = require("./../models/Categories");
const passport = require("passport");
const auth = require("./../authorization");

// index ( c - jake cualing pante)
router.get("/",passport.authenticate("jwt",{session:false}),auth,(req,res,next)=>{
	Category.find()
	.then(categories=>{
		res.json(categories);
	})
	.catch(next)
});

// single view
router.get('/:id',auth,(req,res,next) => {
	Category.findOne({ _id : req.params.id})
	.then( category => {
		res.json(category)
	})
	.catch(next)
})

// create
router.post('/create',passport.authenticate("jwt",{session : false}),auth,(req,res,next) => {
	Category.create({ name : req.body.name})
	.then( (category) => {
		res.send(category)
	})
	.catch(next);
})
// update
router.put('/:id',passport.authenticate('jwt',{session : false}),auth,(req,res,next) => {
	
	Category.findOneAndUpdate(
		{ 
			_id : req.params.id
		},
		{
			name : req.body.name
		},
		{
			new : true
		}
		)

	.then( category => res.json(category))
	.catch(next)

})

// delete
router.delete('/:id',passport.authenticate('jwt',{session : false}),auth,(req,res,next) => {
		
	Category.findOneAndDelete({ _id : req.params.id})
	.then( category => res.json(category))
	.catch(next)

})

module.exports = router;