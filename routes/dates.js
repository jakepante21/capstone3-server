const express = require("express");
const router = express.Router();
const Dates = require("../models/Dates");
const multer = require("multer");
const passport = require("passport");
const auth = require("./../authorization");

// create
router.post("/create",passport.authenticate('jwt',{session : false}),auth,(req,res,next)=>{
	Dates.create(req.body)
	.then(day => {
		res.send(day)
	})
	.catch(next)
})

// show all
router.get("/",(req,res,next) => {
	Dates.find()
	.then(dates => {
		res.json(dates)
	})
	.catch(next)
})

// update
router.put("/:id",passport.authenticate('jwt',{session : false}),auth,(req,res,next) => {
	Dates.findByIdAndUpdate(req.params.id , req.body , { new : true})
	.then( day => res.json(day))
	.catch(next)
})

// delete
router.delete('/:id',passport.authenticate('jwt',{session : false}),auth,(req,res,next) => {
	
	Dates.deleteOne({ _id : req.params.id})
	.then( day => {
		res.json(day)
	})
	.catch(next)
})

module.exports = router;