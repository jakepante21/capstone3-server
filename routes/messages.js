const express = require("express");
const router = express.Router();
const Messages = require("../models/Messages");
const multer = require("multer");
const passport = require("passport");
const auth = require("./../authorization");

// index
router.get("/",passport.authenticate('jwt',{session : false}),auth,(req,res,next) => {
	Messages.find()
	.then(messages => {
		res.json(messages)
	})
	.catch(next)
})

// single view
router.get("/:id",passport.authenticate('jwt',{session : false}),auth,(req,res,next) => {
	Messages.findById(req.params.id)
	.then(message => {
		res.json(message)
	})
	.catch(next)
})

// create
router.post("/create",(req,res,next) => {
	let name = req.body.name;
	let email = req.body.email;
	let subject = req.body.subject;
	let message = req.body.message;
	Messages.create({
		name,
		email,
		subject,
		message
	})
	.then(message => {
		res.send(message)
	})
	.catch(next)
})

// delete
router.delete("/:id",passport.authenticate('jwt',{session : false}),auth,(req,res,next) => {
	Messages.deleteOne({ _id : req.params.id})
	.then(message => {
		res.json(message)
	})
	.catch(next)
})

module.exports = router;

