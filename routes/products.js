const express = require("express");
const router = express.Router();
const Product = require("../models/Products");
const Rooms = require("../models/Rooms");
const multer = require("multer");
const passport = require("passport");
const auth = require("./../authorization");

const storage = multer.diskStorage({
	destination : function(req,file,cb){
		cb(null, "public/products")
	},
	filename : function(req,file,cb){
		cb(null, Date.now() + "-" + file.originalname)
	}
})

const upload = multer({ storage : storage });

// index
router.get("/",(req,res,next) => {
	Product.find()
	.then(products => {
		res.json(products)
	})
	.catch(next)
})

// single view
router.get("/:id",(req,res,next) => {
	Product.findById(req.params.id)
	.then( product => {
		res.json(product)
	})
	.catch(next)
})
// create
router.post("/create", upload.single('image'),passport.authenticate('jwt',{session : false}),auth,(req,res,next) => {
	req.body.image = "/public/" + req.file.filename;
	Product.create(req.body)
	.then( (product) => {
		res.send(product)
	})
	.catch(next);
})
// update
router.put("/:id",upload.single('image'),passport.authenticate('jwt',{session : false}),auth,(req,res,next) => {
	let update = {
		...req.body
	}
	if(req.file){
		update = {
			...req.body, image : "/public/" + req.file.filename
		}
	}
	Product.findByIdAndUpdate(req.params.id , update , { new : true})
	.then( product => res.json(product))
	.catch(next)
})

// delete
router.delete('/:id',passport.authenticate('jwt',{session : false}),auth,(req,res,next) => {
	
	Product.deleteOne({ _id : req.params.id})
	.then( product => {
		res.json(product)
	})
	.catch(next)
})

module.exports = router;