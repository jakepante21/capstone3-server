const express = require("express");
const router = express.Router();
const RentedRoom = require("../models/RentedRooms");
const multer = require("multer");
const passport = require("passport");
const auth = require("./../authorization");

// create
router.post("/create",passport.authenticate('jwt',{session : false}),auth,(req,res,next)=>{
	RentedRoom.create(req.body)
	.then(room => {
		res.send(room)
	})
	.catch(next)
})

// show all
router.get("/",(req,res,next) => {
	RentedRoom.find()
	.then(rooms => {
		res.json(rooms)
	})
	.catch(next)
})

// update
router.put("/:id",passport.authenticate('jwt',{session : false}),auth,(req,res,next) => {
	RentedRoom.findByIdAndUpdate(req.params.id , req.body , { new : true})
	.then( room => res.json(room))
	.catch(next)
})

// delete
router.delete('/:id',passport.authenticate('jwt',{session : false}),auth,(req,res,next) => {
	
	RentedRoom.deleteOne({ _id : req.params.id})
	.then( room => {
		res.json(room)
	})
	.catch(next)
})

module.exports = router;