const express = require("express");
const router = express.Router();
const Product = require("./../models/Products");
const Transaction = require("./../models/Transactions");
const User = require("./../models/Users");
const passport = require("passport");
const auth = require("./../authorization");
const stripe = require("stripe")("sk_test_b9HJIdZesoYxVtay3iFo9UQ900OF45ObDE")

router.post("/orders",(req,res,next)=>{
	let orders = req.body.orders;
	let orderIds = [];
	
	orderIds = orders.map(product => {
		return product.id
	});

	Product.find({ _id : orderIds})
	.then( products => {
		let total = 0;
		let newProducts = products.map( product =>{
			let matchedProduct = {};
			orders.map(order =>{
				if(product.id === order.id){
					matchedProduct = {
						_id : product.id,
						name : product.name,
						price : product.price,
						quantity : order.qty,
						subtotal : product.price * order.qty
					}
					
					
				}
			})
			total+=matchedProduct.subtotal;
			return matchedProduct;
		})	
		res.send({
			products : newProducts,
			total
		})
	})
	
})


// create transact
router.post("/",passport.authenticate('jwt',{session : false}),(req,res,next)=>{

	let orders = req.body.orders;
	let orderIds = [];
	
	orderIds = orders.map(product => {
		return product.id
	});

	Product.find({ _id : orderIds})
	.then( products => {
		let total = 0;
		let newProducts = orders.map( order =>{
			let matchedProduct = {};
			products.forEach(product =>{
				if(order.id === product.id){
					matchedProduct = {
						_id : order.roomId,
						productId : product.id,
						roomNumber : order.number,
						type : order.type,
						name : product.name,
						price : product.price,
						days : order.days,
						subtotal : product.price * order.days
					}
					
					
				}
			})
			total+=matchedProduct.subtotal;
			return matchedProduct;
		})	

		let transaction = {
			userId : req.user._id,
			transactionCode : Date.now(),
			total,
			startDate : req.body.startDate,
			endDate : req.body.endDate,
			products : newProducts
		}

		Transaction.create(transaction)
		.then(transaction=>{
			return res.send(transaction)
		})
		.catch(next)
	})



})


// update transac
router.put("/:id",passport.authenticate('jwt',{session : false}),auth,(req,res,next)=>{
	Transaction.findByIdAndUpdate(req.params.id,{ status : req.body.status},{ new : true})
	.then(transaction=>{
		return res.send(transaction)
	})
})


// view
router.get("/",passport.authenticate('jwt',{session : false}),(req,res,next)=>{
	if(req.user.role === "admin"){
		Transaction.find()
		.then(transactions => {
			User.find()
			.then(users => {
				transactions.map( transaction => {
					users.forEach(user => {
						if (transaction.userId == user._id){
							transaction.userId = user.firstname + " " + user.lastname
						}
					})
					return transaction
				})
				res.send(transactions)
			})
		})
	}
	else {
		Transaction.find({userId : req.user._id})
		.then(transactions => {
			transactions.forEach( transaction =>{
				transaction.userId = req.user.firstname + " " + req.user.lastname
			})
				res.send(transactions)
		})
	}
})

// stripe
router.post('/stripe',(req,res,next) => {

	let total = req.body.total;
	
	User.findOne({_id : req.body.userId})
	.then( user => {
		if(!user) {
			res.status(500).send({message: 'Incomplete'})
		} else {
			if(!user.stripeCustomerId){
	            stripe.customers
	            .create({
	                email: user.email,
	            })
	            .then(customer => {
	                return User.findByIdAndUpdate({ _id: user.id},{stripeCustomerId : customer.id}, {new:true})
	            })
	            .then( user => {
	                return stripe.customers.retrieve(user.stripeCustomerId)
	            })
	            .then((customer) => {
	                return stripe.customers.createSource(customer.id, {
	                source: 'tok_visa',
	                });
	            })
	            .then((source) => {
	                return stripe.charges.create({
	                amount: total,
	                currency: 'usd',
	                customer: source.customer,
	                });
	            })
	            .then((charge) => {
	                // New charge created on a new customer
	                console.log(charge)

	                res.send(charge);
	            })
	            .catch((err) => {
	                // Deal with an error
	                res.send(err)
	            });
	        } else {
                stripe.customers.retrieve(user.stripeCustomerId)
                .then((customer) => {
                    return stripe.customers.createSource(customer.id, {
                    source: 'tok_visa',
                    });
                })
                .then((source) => {
                    return stripe.charges.create({
                    amount: total * 100, //multiply by 100 for centavos
                    currency: 'usd',
                    customer: source.customer,
                    });
                })
                .then((charge) => {
                    // New charge created on a new customer
                    // console.log(charge)
                    res.send(charge);
                })
                .catch((err) => {
                    // Deal with an error
                    res.send(err)
                });
            }


		}
	})

})



module.exports = router;
